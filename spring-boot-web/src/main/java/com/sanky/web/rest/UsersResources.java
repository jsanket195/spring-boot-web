package com.sanky.web.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanky.domain.User;

@RestController
@RequestMapping("/api")
public class UsersResources {
	
	public List<User> getAllUsers(){
		
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < 10; i++) 
			users.add(new User(i+1,"Sanket","Joshi","jsanket195@gmail.com","9762155952"));
		return users;
	}
}
